/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env_tools.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cmaublan <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/03 14:54:31 by cmaublan          #+#    #+#             */
/*   Updated: 2014/03/25 21:29:58 by jburet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_env.h"
#include "../libft/libft.h"
#include <stdlib.h>

char	*ft_getenv(t_sh *sh, const char *name)
{
	int	i;

	i = 0;
	while (sh->env[i] != NULL)
	{
		if (strncmp(sh->env[i], name, strlen(name)) == 0
				&& sh->env[i][strlen(name)] == '=')
		{
			return (sh->env[i] + strlen(name) + 1);
		}
		i++;
	}
	return (NULL);
}

int		ft_setenv(t_sh *sh, char *name, char *value)
{
	int		i;

	i = 0;
	if (ft_get_var(sh, name) != NULL)
	{
		while (sh->env[i] != NULL)
		{
			if (strncmp(sh->env[i], name, strlen(name)) == 0)
			{
				ft_strdel(&sh->env[i]);
				sh->env[i] = ft_strjoin(name, "=");
				sh->env[i] = ft_strjoin(sh->env[i], value);
				return (0);
			}
			i++;
		}
	}
	else
	{
		if (!ft_add_var(sh, name, value))
			return (0);
	}
	return (-1);
}

int		ft_add_var(t_sh *sh, char *name, char *value)
{
	int		i;
	char	**new_env;

	i = 0;
	sh->env_size += 1;
	new_env = (char**)ft_memalloc(sizeof(char*) * sh->env_size);
	if (!new_env)
		return (-1);
	while (sh->env[i] != NULL)
	{
		new_env[i] = ft_strdup(sh->env[i]);
		free(sh->env[i]);
		i++;
	}
	free(sh->env);
	new_env[i] = ft_strjoin(name, "=");
	new_env[i] = ft_strjoin(new_env[i], value);
	sh->env = new_env;
	return (0);
}

int		ft_unsetenv(t_sh *sh, char *name)
{
	int		i;
	char	**new_env;

	i = 0;
    sh->env_size -= 1;
    new_env = (char**)ft_memalloc(sizeof(char*) * sh->env_size);
    if (!new_env)
		return (-1);
    while (sh->env[i] != NULL)
    {
		if (ft_strncmp(name, sh->env[i], ft_strlen(name)) == 0)
			ft_strdel(&sh->env[i]);
		else
			new_env[i] = ft_strdup(sh->env[i]);
		free(sh->env[i]);
		i++;
    }
    free(sh->env);
    sh->env = new_env;
    return (0);
}

void	ft_printenv(t_sh *sh)
{
	int	i;

	i = 0;
	while (i < sh->env_size)
	{
		if (sh->env[i] != NULL)
			ft_putendl(sh->env[i]);
		i++;
	}
}
