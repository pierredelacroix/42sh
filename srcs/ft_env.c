/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cmaublan <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/10 15:19:28 by cmaublan          #+#    #+#             */
/*   Updated: 2014/03/25 20:51:55 by cmaublan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_env.h"
#include <dirent.h>
#include <stdlib.h>
#include "../includes/ft_hash.h"

t_sh		*ft_make_sh(t_sh *sh, char **environ)
{
	int		i;

	i = 0;
	sh = (t_sh *)malloc(sizeof(t_sh));
	if (!sh)
		return (NULL);
	while (environ[i] != NULL)
		i++;
	sh->env_size = i;
	sh->env = (char **)ft_memalloc(sizeof(char *) * (sh->env_size + 1));
	if (!sh->env)
		return (NULL);
	i = 0;
	while (environ[i] != NULL)
	{
		sh->env[i] = ft_strdup(environ[i]);
		i++;
	}
	sh->paths = (t_list **)ft_memalloc(sizeof(t_list *) * SIZE_TABLE + 1);
	ft_set_paths(sh);
	return (sh);
}

char		*ft_get_var(t_sh *sh, char *name)
{
	int		i;

	i = 0;
	while (sh->env[i] != NULL)
	{
		if (ft_strncmp(sh->env[i], name, ft_strlen(name)) == 0)
			return (sh->env[i] + (ft_strlen(name) + 1));
		i++;
	}
	return (NULL);
}

void		ft_set_paths(t_sh *sh)
{
	int		i;
	char	**paths;
	DIR		*c_dir;
    struct dirent   *c_file;

	i = 0;
	paths = ft_strsplit(ft_get_var(sh, "PATH"), ':');
	while (paths[i] != '\0')
	{
		paths[i] = ft_strjoin(paths[i], "/");
		c_dir = opendir(paths[i]);
		if (!c_dir)
			return ;
		while ((c_file = readdir(c_dir)) != NULL)
		{
			if (c_file->d_name[0] != '.')
				ft_hash(sh, ft_strjoin(paths[i], c_file->d_name), c_file->d_name);
		}
		free(paths[i]);
		i++;
	}
	free(paths);
}

void		ft_env(t_sh *sh, t_cmd *cmd)
{
	if (cmd)
	{
		if (!cmd->args[1])
			ft_printenv(sh);
		else if (cmd->args[1][0] == '-')
			;
	}
}

void		ft_printenv(t_sh *sh)
{
	int		i;

	i = 0;
	while (i < sh->env_size)
	{
		if (sh->env[i] != NULL)
			ft_putendl(sh->env[i]);
		i++;
	}
}
