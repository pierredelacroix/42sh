/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grammar.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hvillain <hvillain@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/26 08:17:12 by hvillain          #+#    #+#             */
/*   Updated: 2014/03/16 17:02:22 by tmertz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/grammar.h"
#include "../includes/parser.h"

int			ft_grammar_identifier(t_elem *cur, int *status)
{	
	if (!EX_P || TYPE_P != 1)
		*status = 1;
	return (0);
}

int			ft_grammar_subshells(t_elem *cur, int *status)
{
	if ((EX_P && TYPE_P == 0) || (EX_N && TYPE_N == 0))
		return (ft_badly_placed_parenthesis());
	if (EX_P && EX_N && (TYPE_P == 1 || TYPE_N == 1))
		return (ft_too_much_parenthesis());
	else
		*status = 1;
	return (0);
}

int			ft_grammar_redirections(t_elem *cur, int *redir_i, int *redir_o)
{
	if (!EX_N || TYPE_N != 0)
		return (ft_missing_name_redirect());
	if (ft_isin(((t_token *)cur->value)->lexen))
		(*redir_i)++;
	if (ft_isout(((t_token *)cur->value)->lexen))
		(*redir_o)++;
	return (0);
}

int			ft_grammar_pipes(t_elem *cur, int *status, int *redir_i, int *redir_o)
{
	if (*status == 0)
		return (ft_invalid_null_cmd());
	if (*redir_o >= 1)
		return (ft_anbiguous_redirect_out());
	if (*redir_i > 1)
		return (ft_anbiguous_redirect_in());
	ft_check_right_side(cur, status, redir_i, redir_o);
	if (*status == 0)
		return (ft_invalid_null_cmd());
	if (*redir_o > 1)
		return (ft_anbiguous_redirect_out());
	if (*redir_i >= 1)
		return (ft_anbiguous_redirect_in());
	return (0);
}

void		ft_check_right_side(t_elem *cur, int *status, int *redir_i, int *redir_o)
{
	t_elem	*buffer;

	buffer = cur;
	*status = *redir_o = *redir_i = 0;;
	cur = cur->next;
	while (cur != NULL && TYPE != 4 && TYPE != 3)
	{
		if (EX_P && (((TYPE == 0 && TYPE_P != 2) || TYPE == 1)))
			*status = 1;
		if (ft_isin(((t_token *)cur->value)->lexen))
			(*redir_i)++;
		if (ft_isout(((t_token *)cur->value)->lexen))
			(*redir_o)++;
		cur = cur->next;
	}
	cur = buffer;
}

int			ft_grammar_operators(t_elem *cur, int *status, int *redir_i, int *redir_o)
{
	if (ft_strcmp(((t_token *)cur->value)->lexen, "&&")
			|| (EX_P && cur->previous != NULL && TYPE_P != 3 && TYPE_P != 4))
	{
		if (*status == 0)
			return (ft_invalid_null_cmd());
		if (*redir_o > 1)
			return (ft_anbiguous_redirect_out());
		if (*redir_i > 1)
			return (ft_anbiguous_redirect_in());
		ft_check_right_side(cur, status, redir_i, redir_o);
		if (*status == 0)
			return (ft_invalid_null_cmd());
		if (*redir_o > 1)
			return (ft_anbiguous_redirect_out());
		if (*redir_i > 1)
			return (ft_anbiguous_redirect_in());
	}
	return (0);
}

int			ft_grammar_separators(int *status, int *redir_i, int *redir_o)
{
	if (*redir_o > 1)
		return (ft_anbiguous_redirect_out());
	if (*redir_i > 1)
		return (ft_anbiguous_redirect_in());
	*status = *redir_o = *redir_i = 0;
	return (0);
}

int			ft_grammar(t_list *tokens)
{
	t_elem		*cur;
	int			good;
	int			status;
	int			redir_o;
	int			redir_i;

	status = redir_o = redir_i = 0;
	cur = tokens->first;
	while (cur != NULL)
	{
		if (TYPE == 0)
			good = ft_grammar_identifier(cur, &status);
		else if (TYPE == 1)
			good = ft_grammar_subshells(cur, &status);
		else if (TYPE == 2)
			good = ft_grammar_redirections(cur, &redir_i, &redir_o);
		else if (TYPE == 3)
			good = ft_grammar_pipes(cur, &status, &redir_i, &redir_o);
		else if (TYPE == 4)
			good = ft_grammar_operators(cur, &status, &redir_i, &redir_o);
		else if (TYPE == 5)
			good = ft_grammar_separators(&status, &redir_i, &redir_o);
		if (good == -1)
			return (0);
		cur = cur->next;
	}
	return (1);
}

int			ft_isin(char *lexen)
{
	if (!ft_strcmp(lexen, "<") || ! ft_strcmp(lexen, "<<"))
		return (1);
	return (0);
}

int			ft_isout(char *lexen)
{
	if (!ft_strcmp(lexen, ">") || ! ft_strcmp(lexen, ">>"))
		return (1);
	return (0);
}

