/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unsetenv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cmaublan <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/24 11:18:18 by cmaublan          #+#    #+#             */
/*   Updated: 2014/03/25 20:54:09 by cmaublan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_env.h"
#include "../libft/libft.h"
#include <stdlib.h>

int			ft_unsetenv(t_sh *sh,  t_cmd *cmd)
{
	if (cmd)
	{
		{
			if (cmd->args[1] && !cmd->args[2] && cmd->args[1][0] == '-')
				return (ft_unsetvar(sh, cmd->args[1]));
		}
	}
	return (-1);
}

int			ft_unsetvar(t_sh *sh, char *name)
{
	int		i;
    char	**new_env;

    i = 0;
    sh->env_size -= 1;
    new_env = (char**)ft_memalloc(sizeof(char*) * sh->env_size);
    if (!new_env)
		return (-1);
    while (sh->env[i] != NULL)
    {
		if (ft_strncmp(name, sh->env[i], ft_strlen(name)) == 0)
			ft_strdel(&sh->env[i]);
		else
			new_env[i] = ft_strdup(sh->env[i]);
		free(sh->env[i]);
		i++;
    }
    free(sh->env);
    sh->env = new_env;
    return (0);
}
