/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jburet <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/25 20:49:38 by jburet            #+#    #+#             */
/*   Updated: 2014/03/25 21:33:48 by jburet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "history.h"
#include <stdio.h>

void		save_cmd(char *cmd, t_sh *sh)
{
	char	*path;
	int		fd;

	path = ft_strjoin(ft_get_var(sh, "HOME"), "/");
	path = ft_strjoin(path, NAME_HIST);
	printf("path = %s\n", path);
	if ((fd = open(path, O_RDWR | O_CREAT | O_APPEND, 0755)) != -1)
		ft_putendl_fd(cmd, fd);
	else
	{
		printf("fd ret -1\n");
		return ;
	}
	close(fd);
}
