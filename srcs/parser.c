/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmertz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/26 16:24:17 by tmertz            #+#    #+#             */
/*   Updated: 2014/03/25 16:50:05 by tmertz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/parser.h"

t_tree		*ft_parser(t_list *tokens)
{
	t_tree		*tree;

	tree = NULL;
	tree = ft_tree_init(tree);
	if (!ft_grammar(tokens))
		return (NULL);
	tree = ft_parser_make_tree(tree, tokens, NULL, 0);
	return (tree);
}

t_elem		*ft_find_highest_priority(t_list *tokens, int priority)
{
	t_elem		*elem;
	t_elem		*elem2;

	elem = tokens->first;
	if (priority == 1)
		return (NULL);
	if (priority == 2)
	{
		while (elem != NULL && ((t_token *)elem->value)->type != priority)
			elem = elem->next;
		if (elem != NULL)
		{
			elem2 = elem->next;
			while (elem2 != NULL && ((t_token *)elem2->value)->type != priority)
				elem2 = elem2->next;
			if (elem2 != NULL)
				return (elem2);
		}
	}
	else
	{
		while (elem != NULL && ((t_token *)elem->value)->type != priority)
			elem = elem->next;
		if (elem == NULL)
			return (ft_find_highest_priority(tokens, --priority));
	}
	return (elem);
}

void		ft_add_others_to_tree(t_tree *tree, t_list *tokens
									, t_node *node ,int dir)
{
	t_elem		*elem;
	t_list		*list_r;
	t_list		*list_l;
	
	elem = ft_find_highest_priority(tokens, 5);
	if (node == NULL)
		node = ft_add_root(elem, tree);
	else
		node = ft_add_node(node, elem, dir);
	if (((t_token *)elem->value)->type == 2)
	{
		ft_add_redirection_node(node, elem->next, 1);
		ft_list_delone(tokens, elem->next);		
	}
	if (elem->previous)
	{
		list_l = ft_sh_split_list_l(elem->previous, tokens);
		ft_parser_make_tree(tree, list_l, node, 0);
	}
	if (elem->next)
	{
		list_r = ft_sh_split_list_r(elem->next, tokens);
		ft_parser_make_tree(tree, list_r, node, 1);
	}
}

void		ft_add_cmd_to_tree(t_tree *tree, t_list *tokens
									, t_node *node ,int dir)
{
	if (tokens->size == 1 && ((t_token*)tokens->first->value)->type == 1)
		ft_add_node(node, tokens->first, dir);
	if (tree->root == NULL)
		ft_tree_add(tree, ft_tree_new((void *)ft_create_cmd(tokens), 0));
	else
		ft_add_cmd_node(node, ft_create_cmd(tokens), dir);
}

t_tree		*ft_parser_make_tree(t_tree *tree, t_list *tokens
									,t_node *node, int dir)
{
	t_elem		*elem;

	elem = ft_find_highest_priority(tokens, 5);
	if (elem != NULL)
		ft_add_others_to_tree(tree, tokens, node, dir);
	else
	{
		ft_add_cmd_to_tree(tree, tokens, node, dir);
		return (tree);
	}
	return (tree);
}

t_cmd		*ft_create_cmd(t_list *tokens)
{
	t_cmd	*cmd;
	t_elem	*cur;
	int		i;

	cur = tokens->first;
	cmd = ft_memalloc(sizeof(t_cmd));
	cmd->args = ft_memalloc(sizeof(char *) * tokens->size + 1);
	cmd->cmd = cmd->args[0] = ft_strdup(((t_token *)cur->value)->lexen);
	cur = cur->next;
	i = 1;
	while (cur != NULL)
	{
		cmd->args[i++] = ft_strdup(((t_token *)cur->value)->lexen);
		cur = cur->next;
	}
	return (cmd);
}
