/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmertz <tmertz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 22:07:40 by tmertz            #+#    #+#             */
/*   Updated: 2014/03/23 22:24:32 by tmertz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/signals.h"

void	ft_handle_signals(int signum)
{
	if (signum == 11)
		handle_segfault();
}

void	handle_segfault(void)
{
	ft_putendl_fd("SEGFAULLLLLLLLLLT !!!!!", 2);
	ft_unset_term();
	exit(1);
}
