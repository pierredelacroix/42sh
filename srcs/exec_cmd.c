/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_cmd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmertz <tmertz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 16:10:00 by tmertz            #+#    #+#             */
/*   Updated: 2014/03/25 18:32:24 by jburet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/exec_cmd.h"

int		ft_check_if_builltin(char *cmd)
{
	if (!ft_strcmp(cmd, "cd"))
		return (1);
	if (!ft_strcmp(cmd, "exit"))
		return (2);
	if (!ft_strcmp(cmd, "env"))
		return (3);
	if (!ft_strcmp(cmd, "setenv"))
		return(4);
	if (!ft_strcmp(cmd, "unsetenv"))
		return (5);
	if (!ft_strcmp(cmd, "echo"))
		return (6);
	return (0);
}

int		ft_exec_builltin(t_cmd *cmd, t_sh *sh, int nbr)
{
	sh = NULL;
	if (nbr == 6)
		return (ft_echo(cmd));
	if (nbr == 2)
		return (ft_exit(cmd));
	return (0);
}

int		ft_check_cmd(t_node *node, t_sh *sh)
{
	char	*path;
	t_cmd	*cmd;
	int		nbr;

	path = NULL;
	cmd = (t_cmd *)node->value;
	if ((nbr = ft_check_if_builltin(cmd->cmd)) != 0)
		return (ft_exec_builltin(cmd, sh, nbr));
	if (ft_strchr(cmd->cmd, '/') != NULL)
	{
		if (access(cmd->cmd, X_OK) == 0)
			return (ft_exec_cmd(cmd, cmd->cmd, sh));
		else
			return (0);
	}
	else if ((path = get_path_cmd(sh, cmd->cmd)) == NULL)
		return (ft_cmd_not_found(cmd->cmd));
	else
	{
		if (access(path, X_OK) == 0)
			return (ft_exec_cmd(cmd, path, sh));
		else
			return (0);
	}
}

int		ft_exec_cmd(t_cmd *cmd, char *path, t_sh *sh)
{
	pid_t	process;
	int		ret;

	ret = 0;
	process = fork();
	if (process == 0)
			execve(path, cmd->args, sh->env);
	else
		waitpid(process, &ret, 0);
	if (WIFEXITED(ret))
		return (WEXITSTATUS(ret));
	return (1);
}
