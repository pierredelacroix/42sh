/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmertz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/25 14:07:08 by tmertz            #+#    #+#             */
/*   Updated: 2014/03/18 15:24:12 by tmertz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include "../includes/lexer.h"
#include "../includes/lexing_tools.h"
#include "../includes/error_messages.h"

t_list		*ft_lexer(char *line)
{
	int		i;
	t_list	*tokens;

	i = 0;
	tokens = NULL; /*pour la compilation*/
	tokens = ft_list_init(tokens);
	while (line[i] != '\0')
	{
		if (ft_iswhitespace(line[i]))
			i++;
		else if (ft_isseparator(line[i]))
			i += ft_create_token(tokens, ft_strsub(line, i, 1), 5);
		else if (ft_isparenthesis(line[i]))
			i = ft_add_subshell(tokens, line , i);
		else if (ft_isredirection(line[i]))
			i = ft_check_redirection(tokens, line, i);
		else if (ft_isoperator(line[i]))
			i = ft_check_operator(tokens, line, i);
		else
			i = ft_add_word(tokens, line, i);
		if (i == -1)
			return (NULL);
	}
	return (tokens);
}

/* t_sh *sh retiré du prototypage*/
int			ft_add_word(t_list *tokens, char *line, int i)
{
	int		k;
	char	quote;
	char	*result;
	int		status;

	status = k = 0;
	result = ft_memalloc(ft_strlen(line) - i + 1);
	while (line[i] != '\0' && !(ft_iswhitespace(line[i]) && status == 0)
		&& (ft_isletter(line[i]) || status == 1))
	{
		if (status == 1)
		{
			while (line[i] != '\0' && status == 1)
			{
//				else if (line[i] == '$')
//					result = ft_stjoin(result, ft_get_variable(line, i, sh));
				if (line[i] == '\\' && line[i + 1] && line[i + 1] == quote)
				{
					result[k++] = line[i + 1];
					i += 2;
				}
				else if (line[i] == quote)
				{
					status = 0;
					i++;
				}
				else
					result[k++] = line[i++];
			}
		}
		else if (status == 0)
		{
			while (ft_isletter(line[i]) && status == 0)
			{
				if (line[i] == '\\' && line[i + 1])
				{
					result[k++] = line[i + 1];
					i += 2;
				}
				else if (line[i] == '"' || line[i] == '\'')
				{
					quote = line[i++];
					status = 1;
				}
				else
					result[k++] = line[i++];
//				else if (line[i] == '$')
//					result = ft_stjoin(result, ft_get_variable(line, i, sh));
			}
		}
	}
	if (status == 1)
		return (ft_unmatched_quote(quote));
	ft_create_token(tokens, result, 0);
	return (i);
}

int			ft_add_subshell(t_list *tokens, char *line, int i)
{
	int		j;
	int		flag;

	j = ++i;
	flag = 0;
	while (line[i] != '\0')
	{
		if (line[i] == '(')
			flag++;
		else if (line[i] == ')' && flag != 0)
			flag--;
		else if (line[i] == ')' && flag == 0)
		{
			ft_create_token(tokens, ft_strsub(line, j, i - j), 1);
			i++;
			return (i);
		}
		i++;
	}
	if (line[i] == '\0')
	{
		ft_too_much_parenthesis();
		return (-1);
	}
	return (i);
}

int			ft_create_token(t_list *tokens, char *lex, int type)
{
	t_elem		*elem;
	t_token		*token;
	int			i;

	i = ft_strlen(lex);
	token = ft_init_token(lex, type);
	elem = ft_list_new(token, sizeof(t_token *));
	if (tokens->first == NULL)
		ft_list_add(tokens, elem);
	else
		ft_list_push(tokens, elem);
	return (i);
}

int			ft_check_redirection(t_list *tokens, char *line, int i)
{
	if (line[i] == line[i + 1])
		i += ft_create_token(tokens, ft_strsub(line, i, 2), 2);
	else if (line[i + 1] && ft_isredirection(line[i + 1]))
	{
		ft_parse_error(line[i + 1]);
				return (-1);
	}
	else
		i += ft_create_token(tokens, ft_strsub(line, i, 1), 2);
	return (i);
}

int			ft_check_operator(t_list *tokens, char *line, int i)
{
	if (line[i + 1] && line[i + 1] == line[i])
		i += ft_create_token(tokens, ft_strsub(line, i, 2), 4);
	else if (line[i + 1]  && line[i + 1] != line[i])
		i += ft_create_token(tokens, ft_strsub(line, i, 1), 3);
	else
	{
		ft_parse_error(line[i + 1]);
		return (-1);
	}
	return (i);
}

t_token		*ft_init_token(char *lex, int type)
{
	t_token		*new;

	if (!(new = malloc(sizeof(t_token *))))
		return (NULL);
	new->lexen = ft_strdup(lex);
	new->type = type;
	return (new);
}
