/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_messages.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmertz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/25 19:14:38 by tmertz            #+#    #+#             */
/*   Updated: 2014/03/22 16:12:58 by tmertz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/error_messages.h"


void	ft_parse_error(char c)
{
	ft_putstr_fd("Parse error near ", 2);
	if (c == '\0')
		ft_putendl_fd("\n", 2);
	else
		ft_putendl_fd(&c, 2);
}

int		ft_too_much_parenthesis(void)
{
	ft_putendl_fd("Too many ('s.", 2);
	return (-1);
}

int		ft_badly_placed_parenthesis(void)
{
	ft_putendl_fd("Badly placed ( )'s.", 2);
	return (-1);
}

int		ft_invalid_null_cmd(void)
{
	ft_putendl_fd("Invalid null command.", 2);
	return (-1);
}

int		ft_missing_name_redirect(void)
{
	ft_putendl_fd("Missing name for redirect.", 2);
	return (-1);
}

int		ft_unmatched_quote(char c)
{
	ft_putstr_fd("Unmatched ", 2);
	ft_putchar_fd(c, 2);
	ft_putendl_fd(".", 2);
	return (-1);
}

int		ft_anbiguous_redirect_out(void)
{
	ft_putendl_fd("Ambiguous output redirect", 2);
	return (-1);
}

int		ft_anbiguous_redirect_in(void)
{
	ft_putendl_fd("Ambiguous input redirect", 2);
	return (-1);
}

int		ft_cmd_not_found(char *cmd)
{
	ft_putstr_fd(cmd, 2);
	ft_putendl_fd(": Command not found.", 2);
	return (0);
}

int		ft_nofile(char *file)
{
	ft_putstr_fd(file, 2);
	ft_putendl_fd(": No such file or directory.", 2);
	return (0);
}
