/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pde-lacr <pde-lacr@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/24 20:34:24 by pde-lacr          #+#    #+#             */
/*   Updated: 2014/03/23 22:20:48 by tmertz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include <unistd.h>
# include "../libft/libft.h"
# include "signals.h"

# define __UNUSED__ __attribute__((__unused__))

typedef struct			s_sh
{
	t_list		**paths;
	char		**env;
	int			env_size;
	int			in;
	int			out;
}						t_sh;

# include "exec.h"

#endif /* !MAIN_H */
