/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jburet <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/25 20:51:20 by jburet            #+#    #+#             */
/*   Updated: 2014/03/25 21:06:10 by jburet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HISTORY_H
# define HISTORY_H
# include "main.h"
# include "ft_env.h"
# define NAME_HIST ".42sh_history"

void		save_cmd(char *cmd, t_sh *sh);


#endif /*HISTORY_H*/
