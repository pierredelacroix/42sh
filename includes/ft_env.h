/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*  ft_env.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cmaublan <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/28 17:58:09 by cmaublan          #+#    #+#             */
/*   Updated: 2014/03/25 21:31:56 by jburet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_ENV_H
# define FT_ENV_H
# include "../libft/libft.h"
# include "main.h"

/* TOOLS ENV */

char			*ft_getenv(t_sh *sh, const char *name);
t_sh			*ft_make_sh(t_sh *sh, char **environ);
char			*ft_get_var(t_sh *sh, char *name);
void			ft_set_paths(t_sh *sh);
void			ft_printenv(t_sh *sh);

/* BUILTIN SETENV */
int				ft_setenv(t_sh *sh, t_cmd *cmd);
int				ft_setvar(t_sh *sh, char *name, char *value);
int				ft_add_var(t_sh *sh, char *name, char *value);

/* BUILTIN UNSETENV */
int				ft_unsetenv(t_sh *sh,  t_cmd *cmd);
int				ft_unsetvar(t_sh *sh, char *name);

/* BUILTIN EXPORT */
int				ft_export(t_sh *sh, char *name);

#endif /* !ENV_TOOLS_H */
