#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cmaublan <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/27 12:38:23 by cmaublan          #+#    #+#              #
#    Updated: 2014/03/25 21:33:22 by jburet           ###   ########.fr        #
#******************************************************************************#

NAME = 42sh

SRCS = main.c\
	ft_env.c\
	ft_setenv.c\
	ft_unsetenv.c\
	lexer.c\
	lexing_tools.c\
	parser.c\
	parsing_tools.c\
	prompt.c\
	sh_struct.c\
	grammar.c \
	error_messages.c\
	errors.c\
	ft_hash.c\
	exec.c\
	exec_cmd.c\
	operators.c\
	subshell.c\
	redirections.c\
	pipe.c\
	echo.c\
	cursor.c\
	term.c\
	builtins.c\
	signals.c\
	history.c

V = 0

SILENCE_1 :=
SILENCE_0 :=@
SILENCE = $(SILENCE_$(V))

INCLUDE = ./includes
CFLAGS = -g -Wall -Werror -Wextra
CC = $(SILENCE)cc -O0
RM = $(SILENCE)rm -rf
MAKE = $(SILENCE)make V=$(V)

SRCPATH = ./srcs/

#-L /usr/lib -ltermcap useless pour cette version#
INCLUDES_C = -L libft -lft -L/usr/lib -ltermcap
INCLUDES_O = -I includes -I../libft

SRC = $(addprefix $(SRCPATH), $(SRCS))
OBJS= $(SRC:.c=.o)

SKIP_1 :=
SKIP_0 := \033[A
SKIP = $(SKIP_$(V))
C = \033[0;33m
U = $(C)[$(NAME)]----->\033[0m

all: $(NAME)

$(NAME):$(OBJS)
	make -C ./libft
	$(CC) -o $(NAME) $(OBJS) $(CFLAGS) $(INCLUDES_C)

%.o: %.c
	@echo "$(U)$(C)[COMPILE: \033[1;31m$<\033[A\033[0m"
	@echo "\033[0;32m"
	$(CC) -o $@ $(CFLAGS) $(INCLUDES_O) -c $<
	@echo "\033[1;31m [.working.]"
	@echo "$(SKIP)\033[2K\033[A\033[2K$(SKIP)"

clean:
	make -C ./libft clean
	@echo "$(U)$(C)[CLEAN]\033[1;32m"
	$(RM) $(OBJS)
	@echo "$(SKIP)$(U)$(C)[CLEAN:\033[1;32m   DONE$(C)]\033[0m"

fclean: clean
	make -C ./libft fclean
	@echo "$(U)$(C)[F-CLEAN]\033[1;32m"
	$(RM) $(NAME)
	@echo "$(SKIP)$(U)$(C)[F-CLEAN:\033[1;32m DONE$(C)]\033[0m"

re: fclean all

.PHONY: clean fclean
